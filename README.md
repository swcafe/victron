Victron Scanner

This code reads data from the
[Victron VE.DIRECT](https://www.victronenergy.com/live/vedirect_protocol:faq)
output of my BMV-712. I wired it directly to my
[CanHat RS485 device](https://www.waveshare.com/rs485-can-hat.htm) on my boat's
Raspberry Pi.

This code writes rrdtool update statements to stdout, which then get piped
into `rrdtool update -`, which updates a round robin database. This then
gets read by cacti, producing some graphs of my energy usage over time.

![SOC Graph](images/SOC.png)

![Amps Graph](images/amps.png)
