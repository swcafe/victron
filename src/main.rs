use termios::*;
use regex::bytes::Regex;
use std::fs::File;
use std::io::Read;
use std::os::unix::io::AsRawFd;
use std::collections::HashMap;

fn render(value: Option<&&[u8]>, scale: f64) -> String {
    if let Some(value) = value {
        if let Ok(value) = String::from_utf8_lossy(value).parse::<f64>() {
            return format!("{}", value / scale);
        }
    }
    "U".to_string()
}

fn main() -> Result<(), std::io::Error> {
    const PORTNAME: &str = "/dev/serial0";
    let field_re = Regex::new(r"(?P<name>.*?)\t(?P<value>.*?)\r").unwrap();

    loop {
        let mut file = File::open(PORTNAME)?;
        let fd = file.as_raw_fd();
        let mut tty = Termios::from_fd(fd)?;

        tty.c_cflag |= CLOCAL | CREAD; /* ignore modem controls */
        tty.c_cflag &= !CSIZE;
        tty.c_cflag |= CS8; /* 8-bit characters */
        tty.c_cflag &= !PARENB; /* no parity bit */
        tty.c_cflag &= !CSTOPB; /* only need 1 stop bit */
        //    tty.c_cflag &= ~CRTSCTS;    /* no hardware flowcontrol */

        /* setup for non-canonical mode */
        tty.c_iflag &= !(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
        tty.c_lflag &= !(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
        tty.c_oflag &= !OPOST;

        /* fetch as many as possible, in one read, but timeout in 0.1s */
        /* typically this only seems to fetch 64b at a time though */
        tty.c_cc[VMIN] = 255;
        tty.c_cc[VTIME] = 1;

        cfsetispeed(&mut tty, B19200)?;
        tcsetattr(fd, TCSANOW, &tty)?;
        let mut buf = [0; 64];
        let mut message = Vec::new();

        loop {
            match file.read(buf.as_mut()) {
                Err(_) => break,
                Ok(len) => {
                    message.extend(&buf[0..len]);
                    let csize = message.len();
                    const MSGEND: &[u8] = b"Checksum\t";
                    if csize > MSGEND.len() && message[csize - MSGEND.len() - 1..csize - 1] == MSGEND[..] {
                        if message.iter().fold(0u8, |sum, &x| sum.wrapping_add(x)) == 0u8 {
                            let pairs: HashMap<&[u8], &[u8]> = field_re
                                .captures_iter(&message)
                                .filter(|cap| {
                                    &cap["name"] == b"V" ||
                                    &cap["name"] == b"I" ||
                                    &cap["name"] == b"CE" ||
                                    &cap["name"] == b"SOC"
                                })
                                .map(|cap| {
                                    (
                                        cap.name("name").unwrap().as_bytes(),
                                        cap.name("value").unwrap().as_bytes(),
                                    )
                                })
                                .collect();
                            if pairs.get(&b"V"[..]).is_some() {
                                println!("update battery.rrd N:{}:{}:{}:{}",
                                     render(pairs.get(&b"V"[..]), 1000.0),
                                     render(pairs.get(&b"I"[..]), 1000.0),
                                     render(pairs.get(&b"CE"[..]), -1000.0),
                                     render(pairs.get(&b"SOC"[..]), 10.0),
                             );
                            }

                        } else {
                            eprintln!("Message discarded - invalid checksum");
                        }
                        message.clear();
                    }
                    if message.len() > 1024 {
                        // in case we feed in /dev/random
                        message.clear();
                    }
                }
            }
        }
    }
}
